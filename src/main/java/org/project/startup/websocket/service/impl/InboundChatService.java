package org.project.startup.websocket.service.impl;

import reactor.core.publisher.Mono;

import org.project.startup.websocket.service.ChatServiceStreams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;


@Service
@EnableBinding(ChatServiceStreams.class)
public class InboundChatService extends UserParsingHandshakeHandler {

	private final ChatServiceStreams chatServiceStreams;
	
	private  RabbitTemplate rabbitTemplate;
	
	private static final Logger LOG = LoggerFactory.getLogger(InboundChatService.class);

	public InboundChatService(ChatServiceStreams chatServiceStreams){
		this.chatServiceStreams = chatServiceStreams;
	}

	@Override
	protected Mono<Void> handleInternal(WebSocketSession session) {
		return session
			.receive()
			.doOnNext(mes -> {
						
						//read the message
						LOG.info("Received inbound message from client [{}]: {}", session.getId(), mes.getPayloadAsText());
						
						Comment c = new Comment();
						c.setComment("hola");
						
						Mono<Comment> m = Mono.just(c); 
						
						addComment(m);
				
						
						
					})
			.log(getUser(session.getId()) + "-inbound-incoming-chat-message")
			.map(WebSocketMessage::getPayloadAsText)
			.log(getUser(session.getId())+ "-inbound-convert-to-text")
			.flatMap(message ->  broadcast(message, getUser(session.getId())))
			.log(getUser(session.getId()) + "-inbound-broadcast-to-broker")
			.then();
	}

	public Mono<?> broadcast(String message, String user) {
		
		LOG.info("Send message to broken from user [{}]: {}", user, message);
		return Mono.fromRunnable(() -> {
			chatServiceStreams.clientToBroker().send(
				MessageBuilder
					.withPayload(message)
					.setHeader(ChatServiceStreams.USER_HEADER, user)
					.build());
		});
	}
	
	public Mono<String> addComment(Mono<Comment> newComment) {
		
		
		System.out.println("FDDsfsdFFFFDSf");
		return newComment.flatMap(comment ->
		Mono.fromRunnable(() -> rabbitTemplate
		.convertAndSend(
		"learning-spring-boot",
		"comments.new",
		comment)))
		.log("commentService-publish")
		.then(Mono.just("redirect:/"));
		}

}
